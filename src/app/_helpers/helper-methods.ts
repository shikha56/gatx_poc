import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, AlertController, Platform, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

/*
  Generated class for the HelperMethodsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export enum ConnectionStatusEnum {
  Online,
  Offline
}

@Injectable()
export class HelperMethodsProvider {

  previousStatus: ConnectionStatusEnum;
  loading: any;

    constructor(public http: HttpClient, 
                public loadingController: LoadingController,
                private toastController: ToastController,
                private alertController: AlertController,
                public plt: Platform,) {

             this.previousStatus = ConnectionStatusEnum.Online;
    }


    async showMessage(type, msg) {
        const toast = await this.toastController.create({
          message: msg,
          duration: 2000
        });
        toast.present();
    }
    
    async presentLoading() {
        const loading = await this.loadingController.create({
            message: 'Please wait...',
            duration: 1000
        });
        await loading.present();
    }


    async CustomPresentLoading() {
        const loading = await this.loadingController.create({
            message: 'Creating Record, Please wait..',
            duration: 6000
        });
        await loading.present();
    }

    async presentAlert(message: any) {
      const alert = await this.alertController.create({
        message: message,
        buttons: ['Dismiss']
      });
      alert.present();
    }
}