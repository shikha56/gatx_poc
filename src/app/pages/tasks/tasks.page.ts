import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
})
export class TasksPage implements OnInit {

  taskList: any;
  name: any;
  data: any = [];

  constructor(private router: Router,
              private activateRoute: ActivatedRoute) { 

          this.name = this.activateRoute.snapshot.paramMap.get('name');

          // console.log(this.name);
  }

  ngOnInit() {

    this.itemList();
  }

  itemList(){

    if(this.name == 'Physical Inspection'){

      this.taskList = [
        {
          taskname: 'RK001',
          description: 'Project created by John ',
          created_date: '12-05-2020',
          created_by: 'John',
          status: 'Pending'
        },
        {
          taskname: 'RK002',
          description: 'Project created by Daniel ',
          created_date: '12-05-2020',
          created_by: 'Daniel',
          status: 'Completed'
        },
        {
          taskname: 'RK003',
          description: 'Project created by Amit',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
        {
          taskname: 'RK004',
          description: 'Project created by Ankesh ',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
      ];
    }
    else if(this.name == 'New Wagon Inspection'){
      this.taskList = [
        {
          taskname: 'RK001',
          description: 'Project_2 created by John ',
          created_date: '12-05-2020',
          created_by: 'John',
          status: 'Pending'
        },
        {
          taskname: 'RK002',
          description: 'Project_2 created by Daniel ',
          created_date: '12-05-2020',
          created_by: 'Daniel',
          status: 'Completed'
        },
        {
          taskname: 'RK003',
          description: 'Project_2 created by Amit',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
        {
          taskname: 'RK004',
          description: 'Project_2 created by Amit ',
          created_date: '22-05-2020',
          created_by: 'Amit',
          status: 'Completed'
        },
        {
          taskname: 'RK005',
          description: 'Project_2 created by Anil',
          created_date: '04-08-2020',
          created_by: 'Anil',
          status: 'In-Process'
        },
        {
          taskname: 'RK006',
          description: 'Project_2 created by Ankesh ',
          created_date: '04-08-2020',
          created_by: 'Rohit',
          status: 'Pending'
        },
      ];
    }
    else if(this.name == 'Detail Inspection'){
      this.taskList = [
        {
          taskname: 'RK001',
          description: 'Project_2 created by John ',
          created_date: '12-05-2020',
          created_by: 'John',
          status: 'Pending'
        },
        {
          taskname: 'RK002',
          description: 'Project_2 created by Daniel ',
          created_date: '12-05-2020',
          created_by: 'Daniel',
          status: 'Completed'
        },
        {
          taskname: 'RK003',
          description: 'Project_2 created by Amit',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
        {
          taskname: 'RK004',
          description: 'Project_2 created by Amit ',
          created_date: '22-05-2020',
          created_by: 'Amit',
          status: 'Completed'
        },
        {
          taskname: 'RK005',
          description: 'Project_2 created by Anil',
          created_date: '04-08-2020',
          created_by: 'Anil',
          status: 'In-Process'
        },
      ];
    }
    else if(this.name == 'Preview'){

      if(localStorage.getItem("draft") != null){
         this.taskList = JSON.parse(localStorage.getItem("draft"));

         console.log(this.taskList);      
      }
      else{
        this.taskList = [];
      }
    }
    else if(this.name == 'Reports'){
      this.taskList = [
        {
          taskname: 'RK001',
          description: 'Project_2 created by John ',
          created_date: '12-05-2020',
          created_by: 'John',
          status: 'Pending'
        },
        {
          taskname: 'RK002',
          description: 'Project_2 created by Daniel ',
          created_date: '12-05-2020',
          created_by: 'Daniel',
          status: 'Completed'
        },
        {
          taskname: 'RK003',
          description: 'Project_2 created by Amit',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
        {
          taskname: 'RK004',
          description: 'Project_2 created by Amit ',
          created_date: '22-05-2020',
          created_by: 'Amit',
          status: 'Completed'
        },
        {
          taskname: 'RK005',
          description: 'Project_2 created by Anil',
          created_date: '04-08-2020',
          created_by: 'Anil',
          status: 'In-Process'
        },
        {
          taskname: 'RK006',
          description: 'Project_2 created by Ankesh ',
          created_date: '04-08-2020',
          created_by: 'Rohit',
          status: 'Pending'
        },
      ];
    }
  }

  viewTask(taskname, name, taskData){

    this.data = taskData;
    // console.log(this.data);

    let navigationExtras: NavigationExtras = {
      state: {
        data: this.data,
        taskname: taskname,
        name: name
      }
    };
    this.router.navigate(['inspection'], navigationExtras);

  }
}
