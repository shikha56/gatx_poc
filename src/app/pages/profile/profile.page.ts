import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  loginData: any;
  role: any;

  constructor(private router: Router) { 

  }

  ngOnInit() {
    if(localStorage.getItem('loginData')){
      this.loginData = JSON.parse(localStorage.getItem('loginData'));
    }
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['login']);
    localStorage.clear()
    this.router.navigateByUrl('/login');
  }

}
