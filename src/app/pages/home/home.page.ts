import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  inspectionType: any;
  data: any = [];
  taskList = [
    {
      taskname: 'RK001',
      description: 'Project_1 created by John ',
      created_date: '12-05-2020',
      created_by: 'John',
      status: 'Pending'
    },
    {
      taskname: 'RK002',
      description: 'Project_1 created by Daniel ',
      created_date: '12-05-2020',
      created_by: 'Daniel',
      status: 'Completed'
    },
    {
      taskname: 'RK003',
      description: 'Project_1 created by Amit',
      created_date: '12-05-2020',
      created_by: 'Amit',
      status: 'In-Process'
    },
    {
      taskname: 'RK004',
      description: 'Project_1 created by Amit ',
      created_date: '12-05-2020',
      created_by: 'Amit',
      status: 'In-Process'
    },
  ];

  constructor(private router: Router,
              private activateRoute: ActivatedRoute) {

                this.inspectionType = 'physical';
  }

  ngOnInit(){

  }

  checkValue(event){
    console.log(event.detail.value);

    if(event.detail.value == 'physical'){
      this.taskList = [
        {
          taskname: 'RK001',
          description: 'Project_1 created by John ',
          created_date: '12-05-2020',
          created_by: 'John',
          status: 'Pending'
        },
        {
          taskname: 'RK002',
          description: 'Project_1 created by Daniel ',
          created_date: '12-05-2020',
          created_by: 'Daniel',
          status: 'Completed'
        },
        {
          taskname: 'RK003',
          description: 'Project_1 created by Amit',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
        {
          taskname: 'RK004',
          description: 'Project_1 created by Amit ',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
      ];
    }
    if(event.detail.value == 'newwagon'){
      this.taskList = [
        {
          taskname: 'RK001',
          description: 'Project_2 created by John ',
          created_date: '12-05-2020',
          created_by: 'John',
          status: 'Pending'
        },
        {
          taskname: 'RK002',
          description: 'Project_2 created by Daniel ',
          created_date: '12-05-2020',
          created_by: 'Daniel',
          status: 'Completed'
        },
        {
          taskname: 'RK003',
          description: 'Project_2 created by Amit',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
        {
          taskname: 'RK004',
          description: 'Project_2 created by Amit ',
          created_date: '22-05-2020',
          created_by: 'Amit',
          status: 'Completed'
        },
        {
          taskname: 'RK005',
          description: 'Project_2 created by Anil',
          created_date: '04-08-2020',
          created_by: 'Anil',
          status: 'In-Process'
        },
        {
          taskname: 'RK006',
          description: 'Project_2 created by Rohit ',
          created_date: '04-08-2020',
          created_by: 'Rohit',
          status: 'Pending'
        },
      ];
    }

    if(event.detail.value == 'detail'){
      this.taskList = [
        {
          taskname: 'RK001',
          description: 'Project_2 created by John ',
          created_date: '12-05-2020',
          created_by: 'John',
          status: 'Pending'
        },
        {
          taskname: 'RK002',
          description: 'Project_2 created by Daniel ',
          created_date: '12-05-2020',
          created_by: 'Daniel',
          status: 'Completed'
        },
        {
          taskname: 'RK003',
          description: 'Project_2 created by Amit',
          created_date: '12-05-2020',
          created_by: 'Amit',
          status: 'In-Process'
        },
        {
          taskname: 'RK004',
          description: 'Project_2 created by Amit ',
          created_date: '22-05-2020',
          created_by: 'Amit',
          status: 'Completed'
        },
        {
          taskname: 'RK005',
          description: 'Project_2 created by Anil',
          created_date: '04-08-2020',
          created_by: 'Anil',
          status: 'In-Process'
        },
      ];
    }
  }

  viewTask(taskname, name, taskData){

    this.data = taskData;
    // console.log(this.data);

    let navigationExtras: NavigationExtras = {
      state: {
        data: this.data,
        taskname: taskname,
        name: name
      }
    };
    this.router.navigate(['inspection'], navigationExtras);
  }
}
