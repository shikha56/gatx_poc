import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  notificationList = [
    {
      name: 'Task has been assigned'
    },
    {
      name: 'Task has been finished'
    },
    {
      name: 'Task 322 has been assigned'
    },
    {
      name: 'Task has been completed'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
