import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HelperMethodsProvider } from 'src/app/_helpers/helper-methods';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginFormGroup: FormGroup;
  isSubmitted = false;
  role: any;
  data: any = [];

  constructor(private router: Router, 
              public formBuilder: FormBuilder,
              private helperMethodsProvider: HelperMethodsProvider) { 

      this.loginFormGroup = formBuilder.group({
        username: ['', [Validators.required]],
        password: ['', Validators.required]
      });

  }

  ngOnInit() {
  }

  get errorControl() {
    return this.loginFormGroup.controls;
  }

  // get mobileNo() {
  //   return this.loginFormGroup.get('mobileNo');
  // }

  // get password() {
  //   return this.loginFormGroup.get('password');
  // }
  
  loginForm(){

    this.isSubmitted = true;

    if (!this.loginFormGroup.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.loginFormGroup.value)

      // this.helperMethodsProvider.presentLoading();

      if(this.loginFormGroup.value.username == 'lalu' && this.loginFormGroup.value.password == '123456'){
              this.role = 'Supervisor';
              this.data = {
                  username: this.loginFormGroup.value.username,
                  password: this.loginFormGroup.value.password,
                  fullname: 'Lalu Tyagi',
                  role: this.role,
                  mobileNo: '7048351338',
                  inspection: true,
                  email: 'lalu@gmail.com',
                  address: 'Noida Sector-21',
                }

                localStorage.setItem('loginData', JSON.stringify(this.data));
                this.helperMethodsProvider.showMessage('success', 'Login Successfully');
                this.router.navigateByUrl("/dashboard");          
      }
      else if(this.loginFormGroup.value.username == 'shikha' && this.loginFormGroup.value.password == '123456'){
            this.role = 'Inspector';
            this.data = {
                username: this.loginFormGroup.value.username,
                password: this.loginFormGroup.value.password,
                fullname: 'Shikha Chaurasia',
                role: this.role,
                mobileNo: '9015553916',
                inspection: true,
                email: 'shikha@gmail.com',
                address: 'Noida Sector-15',
              }
            
              localStorage.setItem('loginData', JSON.stringify(this.data));
              this.helperMethodsProvider.showMessage('success', 'Login Successfully');
              this.router.navigateByUrl("/dashboard");
      } 
      else{
          this.helperMethodsProvider.showMessage('danger', 'Invalid Login Credentials');
      }   
    }  
  }
}
