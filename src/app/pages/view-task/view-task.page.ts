import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.page.html',
  styleUrls: ['./view-task.page.scss'],
})
export class ViewTaskPage implements OnInit {

  isMenuOpen : boolean = false;

  actionSheet: any;
  taskname: any;
  name:any;
  data: any;
  segment:any = 'View Info';
  segmentStage: any = 'Stage 1'

  draftdata: any = [];
  draftd: any = [];

  quantity_rake = '124';
  inspectionOfferQ : any;
  clearedQ: any;
  cumulativeQC: any;
  content: any;

  templateList = [{
     headingName: 'CASTINGS',
     templateName:[
      {
        name: 'Striker Casting',
        value: '124'
      },
      {
        name: 'YPS',
        value: '124'
      },
      {
        name: 'Follower',
        value: '124'
      },
      {
        name: 'YOKE',
        value: '124'
      },
      {
        name: 'Bogie',
        value: '124'
      }
     ]
  },
  {
    headingName: 'FABRICATION',
    templateName:[
     {
       name: 'Center Pivot Filler',
       value: '124'
     },
     {
       name: 'Center SillL DP (10%)',
       value: '13'
     },
     {
       name: 'Head Stock',
       value: '248'
     },
     {
       name: 'Center Ridge & Transverse Ridge Assy',
       value: '62'
     },
     {
       name: 'Slide Plate Assembly Fitup With UF',
       value: '992'
     }
    ]
  }];

  constructor(private router: Router,
              private activateRoute: ActivatedRoute,
              private actionSheetController: ActionSheetController) { 

               
                  if (this.router.getCurrentNavigation().extras.state) {

                    this.taskname = this.router.getCurrentNavigation().extras.state.taskname;
                    this.name = this.router.getCurrentNavigation().extras.state.name;
                    this.data = this.router.getCurrentNavigation().extras.state.data;
                  }
                
                console.log("data---",this.data);

                this.inspectionOfferQ = this.data.inspecOfferQ;
                this.clearedQ = this.data.cleareQuan;
                this.cumulativeQC = this.data.cumulativeQuan;
                this.content = this.data.remark;
  }

  ngOnInit() {
  }

  toggleAccordion(){
    this.isMenuOpen = !this.isMenuOpen;
  }

  presentActionSheet(quantity_rake, inspectionOfferQ, clearedQ, cumulativeQC) {
    // console.log(quantity_rake);
    this.actionSheet = this.actionSheetController.create({
      // header: 'Freaky Jolly',
      cssClass: 'action-sheet',
      buttons: [{
        text: 'Save as draft',
        role: 'destructive',
        icon: 'archive',
        handler: () => {

          // console.log(quantity_rake);
          
          this.draftdata = {
            taskname:  this.data.taskname,
            description: this.data.description,
            created_date: this.data.created_date,
            created_by: this.data.created_by,
            status: this.data.status,
            quantityrake: quantity_rake,
            inspecOfferQ: inspectionOfferQ,
            cleareQuan: clearedQ,
            cumulativeQuan: cumulativeQC,
            remark: this.content
          }

          if(localStorage.getItem("draft") != null){
            this.draftd = JSON.parse(localStorage.getItem("draft")).concat(this.draftdata);                  
          }
          else{
            this.draftd.push(this.draftdata);
          }
         
          console.log(this.draftd);
          localStorage.setItem("draft",JSON.stringify(this.draftd));
          console.log('Save as draft');
        }
      }, {
        text: 'Save as final',
        icon: 'save',
        cssClass: 'action-sheet-final',
        handler: () => {
          console.log('Save as final');
        }
      }, {
        text: 'Cancel',
        icon: 'close-circle',
        role: 'cancel',
        cssClass: 'action-sheet-final',
        handler: () => {
          console.log('Close');
        }
      }]
    }).then(actionsheet => {
      actionsheet.present();
    });
  }
}
