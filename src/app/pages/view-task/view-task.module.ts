import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ViewTaskPageRoutingModule } from './view-task-routing.module';
import { ViewTaskPage } from './view-task.page';
// import { MiAccordionPage } from '../mi-accordion/mi-accordion.page'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewTaskPageRoutingModule
  ],
  declarations: [ViewTaskPage],
  // schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewTaskPageModule {}
