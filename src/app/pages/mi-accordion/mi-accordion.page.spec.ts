import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MiAccordionPage } from './mi-accordion.page';

describe('MiAccordionPage', () => {
  let component: MiAccordionPage;
  let fixture: ComponentFixture<MiAccordionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiAccordionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MiAccordionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
