import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MiAccordionPageRoutingModule } from './mi-accordion-routing.module';

import { MiAccordionPage } from './mi-accordion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MiAccordionPageRoutingModule
  ],
  declarations: [MiAccordionPage]
})
export class MiAccordionPageModule {}
