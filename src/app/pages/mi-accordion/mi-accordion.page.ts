import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mi-accordion',
  templateUrl: './mi-accordion.page.html',
  styleUrls: ['./mi-accordion.page.scss'],
})
export class MiAccordionPage implements OnInit {
  @Input()
  heading: any;

  @Input()
  optionName: any;

  @Input()
  optionValue: any;
  
  isMenuOpen : boolean = false;

  constructor() { 

  }

  ngOnInit() {
  }

  toggleAccordion(){
      this.isMenuOpen = !this.isMenuOpen;
  }
}
