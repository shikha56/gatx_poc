import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MiAccordionPage } from './mi-accordion.page';

const routes: Routes = [
  {
    path: '',
    component: MiAccordionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MiAccordionPageRoutingModule {}
