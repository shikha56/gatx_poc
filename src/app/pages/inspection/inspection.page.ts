import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { HelperMethodsProvider } from 'src/app/_helpers/helper-methods';

@Component({
  selector: 'app-inspection',
  templateUrl: './inspection.page.html',
  styleUrls: ['./inspection.page.scss'],
})
export class InspectionPage implements OnInit {
  taskname: any;
  name:any;
  data: any;

  draftdata: any = [];
  draftd: any = [];

  project_rake: any;
  inspectonItem: any;
  inspectonCategory: any;
  inspectonCategoryValue: boolean = false;

  inspectonItemList: any;

  quantity_rake = '124';
  balanceQ = '124';
  balanceQPer = '100%';
  inspectionOfferQ : any;
  clearedQ: any;
  cumulativeQC: any;
  remark: any;

  fileData: File = null;
  document: any = null;
  documentData:any = null;

  customPopoverOptions: any = {
    header: 'Select Inspection'
  };

  constructor(private router: Router,
              private activateRoute: ActivatedRoute,
              private helperMethodsProvider: HelperMethodsProvider) { 

          if (this.router.getCurrentNavigation().extras.state) {

            this.taskname = this.router.getCurrentNavigation().extras.state.taskname;
            this.name = this.router.getCurrentNavigation().extras.state.name;
            this.data = this.router.getCurrentNavigation().extras.state.data;
          }
  }

  ngOnInit() {
    if(localStorage.getItem("draft") != null){
      var draftData = JSON.parse(localStorage.getItem("draft"));
      this.data = this.router.getCurrentNavigation().extras.state.data;
      console.log(this.data);      

      this.project_rake = this.data.projectRake;
      this.inspectonItem = this.data.inspectonItem;
      this.inspectonCategory = this.data.inspectonCategory;
      this.inspectonCategoryValue = true;
      this.inspectionOfferQ = this.data.inspecOfferQ;
      this.clearedQ = this.data.cleareQuan;
      this.cumulativeQC = this.data.cumulativeQuan;
      this.remark = this.data.remark;
      this.document = this.data.document;
    }
  }

  checkInpection(value){
    console.log(this.project_rake);
    
  }

  checkInspectonCategory(value){

    if(value == 'Castings'){
      
      this.inspectonItemList = [
         {
          id: 1,
          name: 'Striker Casting',
          value: '124'
         },
         {
          id: 2,
          name: 'YPS',
          value: '124'
         },
         {
          id: 3,
          name: 'Follower',
          value: '124'
         },
         {
          id: 4,
          name: 'YOKE',
          value: '124'
         },
         {
          id: 5,
          name: 'Bogie',
          value: '124'
         }
      ];
    }
    else if(value == 'Fabrication'){
      this.inspectonItemList = [
        {
          id: 1,
          name: 'Center Pivot Filler',
          value: '124'
        },
        {
          id: 2,
          name: 'Center SillL DP (10%)',
          value: '13'
        },
        {
          id: 3,
          name: 'Head Stock',
          value: '248'
        },
        {
          id: 4,
          name: 'Center Ridge & Transverse Ridge Assy',
          value: '62'
        },
        {
          id: 5,
          name: 'Slide Plate Assembly Fitup With UF',
          value: '992'
        }
      ];
    }
  }

  checkInspectonItem(value){
    console.log(this.inspectonItem);
    this.inspectonCategoryValue = true;

  }

  fileSelected(fileInput: any){
    this.fileData = <File>fileInput.target.files[0]; 
    this.preview(); 
  }

  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();      
    reader.readAsDataURL(this.fileData); 
    reader.onload = (_event) => { 
      this.documentData = reader.result; 
      this.document = this.fileData.name;
    }
  }

  draft(projectrakeValue, inspectionCategoryValue, inspectonItemValue, documents){
    this.inspectonCategoryValue = false;

    this.draftdata = {
      taskname:  this.data.taskname,
      description: this.data.description,
      created_date: this.data.created_date,
      created_by: this.data.created_by,
      status: this.data.status,
      projectRake: projectrakeValue,
      inspectonCategory: inspectionCategoryValue,
      inspectonItem: inspectonItemValue,
      document: this.document,
      inspecOfferQ: this.inspectionOfferQ,
      cleareQuan: this.clearedQ,
      cumulativeQuan: this.cumulativeQC,
      remark: this.remark
    }

    if(localStorage.getItem("draft") != null){
      this.draftd = JSON.parse(localStorage.getItem("draft")).concat(this.draftdata);                  
    }
    else{
      this.draftd.push(this.draftdata);
    }
   
    console.log(this.draftd);
    localStorage.setItem("draft",JSON.stringify(this.draftd));
    this.helperMethodsProvider.showMessage('success', 'Draft Successfully');
    
    this.reset();
  }

  save(projectrakeValue, inspectionCategoryValue, inspectonItemValue, documents){
    this.inspectonCategoryValue = false;

    this.draftdata = {
      taskname:  this.data.taskname,
      description: this.data.description,
      created_date: this.data.created_date,
      created_by: this.data.created_by,
      status: this.data.status,
      projectRake: projectrakeValue,
      inspectonCategory: inspectionCategoryValue,
      inspectonItem: inspectonItemValue,
      document: documents,
      inspecOfferQ: this.inspectionOfferQ,
      cleareQuan: this.clearedQ,
      cumulativeQuan: this.cumulativeQC,
      remark: this.remark
    }

    if(localStorage.getItem("draft") != null){
      this.draftd = JSON.parse(localStorage.getItem("draft")).concat(this.draftdata);                  
    }
    else{
      this.draftd.push(this.draftdata);
    }
   
    console.log(this.draftd);
    localStorage.setItem("draft",JSON.stringify(this.draftd));
    this.helperMethodsProvider.showMessage('success', 'Save Successfully');
    this.reset();
  }

  reset(){
      this.project_rake = '';
      this.inspectonItem = '';
      this.inspectonCategory = '';
      this.inspectonCategoryValue = false;

      this.inspectionOfferQ = '';
      this.clearedQ = '';
      this.cumulativeQC = '';
      this.remark = '';

      this.fileData = null;
      this.document = '';
      this.documentData = '';

  }

  generateSheet(){

  }
}
