import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  loginData: any;
  role: any;

  constructor(private router: Router) { 

  }

  ngOnInit() {

    if(localStorage.getItem('loginData')){
        this.loginData = JSON.parse(localStorage.getItem('loginData'));
        this.role = this.loginData.role;
    }
  }

}
                                                                                                 